var ViewModel = {

    _gpx: null,
    _training: null,
    init: function() {

        GPXParser.process = this.process.bind(this);

        GPXParser.makeRequest("GET","Afternoon_Run.gpx").then(function(response){
            GPXParser.parseFile(new Blob([response]));
        }).catch(function(error) {
            console.log(error);
        });
    },

    onFileSelect: function(files) {

        GPXParser.parseFile(files[0]);
    },

    process: function(data) {
        console.assert(data.trk != null, "Fix parser");

        this._gpx = Object.create(GPXdata);
        this._training = Object.create(TrainingEffect);
        this._training.tzones = [0, 0, 0, 0, 0];

        var segments = [];
        //merge combined tracks
        if (Array.isArray(data.trk)) {
            var trkseg = [];
            data.trk.forEach(function (segment) {
                if (segment.name && !data.trk.name) {
                    data.trk.name = segment.name;
                }
                trkseg.push(segment.trkseg);
            });
            data.trk.trkseg = trkseg;
        }

        //check for segments
        if (Array.isArray(data.trk.trkseg)) {
            data.trk.trkseg.forEach(function (segment) {
                segments = segments.concat(segment.trkpt);
            });

        } else {
            segments = data.trk.trkseg.trkpt;
        }

        this._gpx.setName(data.trk.name);

        /*{
            @lat:"43.0811410"
            @lon:"25.6122170"
            ele:"256.2"
            extensions:gpxtpx:TrackPointExtension:gpxtpx:hr:"112"
            time:"2016-08-28T11:57:39Z"
        }*/
        var prev = null;
        var geoJsonFeature =  { "type": "LineString", "coordinates": [] };
        for (var index = 0; index < segments.length; index++) {
            var segment = segments[index];
            this.processSegment(prev, segment, index);
            geoJsonFeature.coordinates.push([segment['@lon'], segment['@lat']]);
            prev = segment;
        }

        this.dataParsed();
        this.loadGeoJSON(geoJsonFeature);
    },

    processSegment: function(prev, segment, index) {
        var bpm = 0;
        this._gpx.count = index;
        if(segment.extensions) {
            var extensions = segment.extensions['gpxtpx:TrackPointExtension'];
            bpm = extensions['gpxtpx:hr'];
            this._gpx.setHeartRate(bpm);
        }

        if (prev) {
            var p1 = new L.latLng(prev['@lat'], prev['@lon'], prev.ele);
            var p2 = new L.latLng(segment['@lat'], segment['@lon'], segment.ele);

            this._gpx.setDistance(p1.distanceTo(p2));
            this._gpx.setAlt(prev.ele, segment.ele);
            this._gpx.setTimeDelta(prev.time, segment.time);
            this._training.calcSS(prev.time, segment.time, bpm);
        }
    }
};
