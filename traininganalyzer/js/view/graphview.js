var GraphView = {

    init: function () {

        this.attachEvents();
        //this.viewModel.dataParsed = this.dataParsed.bind(this);

    },

    attachEvents: function () {

    },

    showHrChart: function() {
        var ctx = document.getElementById("hrZonesChart");
        var data = {
        labels: ["Z1", "Z2", "Z3", "Z4", "Z5"],
            datasets: [
                {
                    label: "HRZones",
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)'

                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)'
                    ],
                    borderWidth: 1,
                    data: []
                }
            ]
        };

        var min = this.viewModel._gpx.getMin();
        for(var i = 0; i < 5; i++) {
            data.datasets[0].data[i] = (((this.viewModel._training.tzones[i] / 1000) / 60)/ min) * 100;
        }

        this.hrChart = new Chart(ctx, {
            type: 'horizontalBar',
            data: data,

            options: {
                responsive: true,
                maintainAspectRatio: false,

                scales: {
                    xAxes: [{
                        stacked: false
                    }],
                    yAxes: [{
                        stacked: false
                    }]
                }
            }
        });

    document.getElementById("hrZonesChartLegend").innerHTML = "<ul>"+
        "<li>Suffer score: "+this.viewModel._training.getSS().sufferScore+"</li>" +
        "<li>Red points: "+this.viewModel._training.getSS().redPoints+"</li>" +
        "<li>TRIMP: "+ this.viewModel._training.getTRIMP(min, this.viewModel._gpx.getHeartRateAvg()) + "</li>"+
        "<li>TE: "+ this.viewModel._training.calcTE( this.viewModel._gpx.getHeartRateAvg(),  this.viewModel._gpx.hrMax) + "</li>" +
        "</ul>";
    },

    showElevationChart: function() {
        var ctx = document.getElementById("elevationChart");
        var data = {
            labels: this.viewModel._gpx.times, //this.viewModel._gpx.elevation,
            datasets: [
                {
                    label: "elevation",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: this.viewModel._gpx.elevation,
                    spanGaps: false,
                },
                {
                    label: "HR",
                    fill: true,
                    lineTension: 0.1,
                    backgroundColor: "rgba(255,0,0,0.4)",
                    borderColor: "rgba(255,0,0,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(255,0,0,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(255,0,0,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    data: this.viewModel._gpx.hrs,
                    spanGaps: false,
                }
            ]
        };

        var myLineChart = new Chart(ctx, {
            type: 'line',
            data: data,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        display: false
                    }]
                }
            }
        });
    }
};