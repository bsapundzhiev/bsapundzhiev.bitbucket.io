﻿'use strict';
var GPXdata = {

    name: null,
    amin: null,
    amax: null,
    distance: null,
    hrMax:0,
    hrAvg:0,
    count: 0,
    millisec: 0,
    elevation: [],
    hrs:[],
    times:[],
    setName: function(name) {
        this.name = name;
    },

    setDistance: function(dist) {
        this.distance += dist;
    },

    setAlt: function(a1, a2) {
        this.elevation.push(a1);
        this.elevation.push(a2);
        if(Number(a2) > Number(a1)) {

            this.amax += a2 - a1;
        } else {

            this.amin += a1 - a2;
        }
    },

    setTimeDelta: function(t1, t2) {
        var date1 = new Date(t1);
        var date2 = new Date(t2);
        this.millisec += (date2.getTime() - date1.getTime());
        var tt = (this.millisec / 1000).toFixed(0) / 60;
        this.times.push(tt);
    },

    setHeartRate: function(hr) {
        this.hrs.push(hr);
        this.hrMax = (hr > this.hrMax) ? hr : this.hrMax;
        this.hrAvg += Number(hr);
    },

    getHeartRateAvg: function() {

        return Math.round(this.hrAvg / this.count);
    },

    getSec: function() {
       return (this.millisec / 1000).toFixed(0);
    },

    getMin: function(sec) {
        var seconds = sec || this.getSec();
        return Math.floor(seconds / 60);
    },

    getTime: function() {
        var seconds = this.getSec();
        var minutes = this.getMin(seconds);
        var hours = "";
        if (minutes > 59) {
            hours = Math.floor(minutes / 60);
            hours = (hours >= 10) ? hours : "0" + hours;
            minutes = minutes - (hours * 60);
            minutes = (minutes >= 10) ? minutes : "0" + minutes;
        }
        seconds = Math.floor(seconds % 60);
        seconds = (seconds >= 10) ? seconds : "0" + seconds;
        if (hours != "") {
            return hours + ":" + minutes + ":" + seconds;
        }
        return minutes + ":" + seconds;
    },

    getDistance: function() {
        return this.distance / 1e3;
    }

};